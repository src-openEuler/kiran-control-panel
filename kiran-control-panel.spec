Name:       kiran-control-panel
Version:    2.6.1
Release:    8%{?dist}
Summary:    Kiran Control Panel
Summary(zh_CN): Kiran桌面控制面板

License:    MulanPSL-2.0
Source0:    %{name}-%{version}.tar.gz

Patch000:   0000-fix-audio-when-the-tray-is-initialized-determine-whe.patch
Patch001:   0001-fix-keybinding-don-t-convert-normal-key-if-use-shift.patch
Patch002:   0002-fix-audio-network-Compatible-for-versions-below-5.15.patch
Patch003:   0003-fix-power-Fix-has-really-battery-while-it-is-present.patch
Patch004:   0004-fix-network-Fix-network-configuration-filtering-for-.patch
Patch005:   0005-fix-touchpad-Fix-touchpad-display-with-the-type-psmo.patch
Patch006:   0006-fix-search-Adjusting-the-display-style-of-manually-r.patch
Patch007:	0007-fix-network-Crash-caused-by-mismatch-between-registe.patch

BuildRequires:    gcc-c++
BuildRequires:	cmake >= 3.2

BuildRequires:    glib2-devel
BuildRequires:    upower-devel
BuildRequires:    zeromq-devel
BuildRequires:    libnotify-devel
BuildRequires:    pam-devel
BuildRequires:    cryptopp-devel
BuildRequires:    libXrandr-devel
BuildRequires:    libXcursor-devel

BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtbase-static 
BuildRequires:  qt5-qtbase-private-devel
BuildRequires:  qt5-qtx11extras-devel
BuildRequires:  qt5-qtsvg-devel
BuildRequires:  qt5-qtmultimedia-devel
BuildRequires:  qt5-linguist
BuildRequires:  kf5-networkmanager-qt-devel

BuildRequires:  kiran-log-qt5-devel
BuildRequires:  kiran-widgets-qt5-devel
BuildRequires:  kiran-qt5-integration-devel
BuildRequires:  kiran-qdbusxml2cpp
BuildRequires:  kiran-cc-daemon-devel
BuildRequires:  kiran-authentication-service-devel >= 2.5
BuildRequires:  libqtxdg-devel
BuildRequires:  kf5-kconfig-devel

Requires:    qt5-qtbase
Requires:    qt5-qtbase-gui
Requires:    qt5-qtx11extras
Requires:    qt5-qtsvg
Requires:    qt5-qtmultimedia
Requires:    kf5-networkmanager-qt

Requires:    kiran-log-qt5
Requires:    kiran-widgets-qt5 >= 2.4
Requires:    kiran-qt5-integration >= 2.4
Requires:    kiran-system-daemon >= 2.4
Requires:    kiran-session-daemon >= 2.4
Requires:    kiran-authentication-service >= 2.5

Requires:    glib2
Requires:    upower
Requires:    zeromq
Requires:    libnotify
Requires:    pam
Requires:    cryptopp
Requires:    libqtxdg
Requires:    kf5-kconfig

Requires:    NetworkManager-l2tp
#Requires:    NetworkManager-pptp

%if "%{ks_custom_name}" == "GC"
Requires:   group-service
%endif

Obsoletes:    kiran-cpanel-account < 2.3
Obsoletes:    kiran-cpanel-appearance < 2.3
Obsoletes:    kiran-cpanel-display < 2.3
Obsoletes:    kiran-cpanel-keybinding < 2.3
Obsoletes:    kiran-cpanel-keyboard < 2.3
Obsoletes:    kiran-cpanel-mouse < 2.3
Obsoletes:    kiran-cpanel-power < 2.3
Obsoletes:    kiran-cpanel-system < 2.3
Obsoletes:    kiran-cpanel-timedate < 2.3

%description
Kiran Control Panel

%package -n kiran-cpanel-launcher
Summary: Kiran control panel plugin launcher
Summary(zh_CN): kiran桌面控制面板启动器

BuildRequires:  gcc-c++
BuildRequires:  cmake >= 3.2
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtx11extras-devel
BuildRequires:  qt5-qtsvg-devel
BuildRequires:  qt5-linguist
BuildRequires:  kiran-log-qt5-devel
BuildRequires:  kiran-widgets-qt5-devel >= 2.4
BuildRequires:  glib2-devel
BuildRequires:  kiran-qt5-integration-devel >= 2.4

Requires:    qt5-qtbase
Requires:    qt5-qtx11extras
Requires:    qt5-qtsvg
Requires:    kiran-log-qt5
Requires:    kiran-widgets-qt5 >= 2.4
Requires:    glib2
Requires:    kiran-qt5-integration >= 2.4

%description -n kiran-cpanel-launcher
%{summary}.

%package devel
Summary: Development files for kiran control panel plugin
%description devel
%{summary}.

%prep
%autosetup -p 1

%build
%cmake
     
%cmake_build

%install
%cmake_install

%files
#主面板
%dir %{_datadir}/kiran-control-panel
%{_bindir}/kiran-control-panel
%{_datadir}/kiran-control-panel/*
%{_datadir}/icons/hicolor/*
%exclude %{_datadir}/applications/kiran-control-panel.desktop
%{_libexecdir}/kiran-avatar-editor


#audio
%{_sysconfdir}/xdg/autostart/kiran-audio-status-icon.desktop
%{_bindir}/kiran-audio-status-icon

#network
%{_sysconfdir}/xdg/autostart/kiran-network-status-icon.desktop
%{_bindir}/kiran-network-status-icon
/etc/NetworkManager/conf.d/00-server.conf


%files -n kiran-cpanel-launcher
%{_bindir}/kiran-cpanel-launcher

%files devel
%dir %{_includedir}/kiran-control-panel
%{_includedir}/kiran-control-panel/*
%{_libdir}/pkgconfig/kiran-control-panel.pc

%changelog
* Wed Nov 20 2024 Funda Wang <fundawang@yeah.net> - 2.6.1-8
- adopt to new cmake macro

* Fri Apr 19 2024 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.6.1-7
- KYOS-B: Fix Crash caused by mismatch between registered search terms and actual content(#35252)
- KYOS-B: Adjusting the display style of manually registered search keywords and subfunctional keywords(#35340,#35278) 

* Wed Apr 17 2024 meizhigang <meizhigang@kylinsec.com.cn> - 2.6.1-6
- KYOS-B: Fix touchpad display with the type psmouse (#34878)

* Tue Apr 16 2024 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.6.1-5
- KYOS-B: Fix network configuration filtering for device binding and settings update issue(#32685)

* Tue Apr 16 2024 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.6.1-4
- sync to openEuler

* Wed Apr 10 2024 meizhigang <meizhigang@kylinsec.com.cn> - 2.6.1-3
- KYOS-B: Compatible for versions below 5.15
- KYOS-B: Fix has really battery while it is present (#31917)

* Tue Mar 5 2024 luoqing <luoqing@kylinsec.com.cn> - 2.6.1-2.kb1
- KYOS-B: when the tray is initialized, determine whether there is asound card,if not, the volume icon is disabled (#22865)
- KYOS-B: fix(keybinding):don't convert normal key if use shift modifier in keybinding (#30978)

* Thu Feb 2 2024 luoqing <luoqing@kylinsec.com.cn> - 2.6.1-1.kb1
- KYOS-B: The network connection icon is displayed only in the fully Connected state.The connection icon is not displayed in the ConnectedLinkLocal state (#27750)
- KYOS-B: fixed an issue where tray page expansion size changed incorrectly when multiple network cards were used (#25239)
- KYOS-B: fixed incorrect connectivity results when frequent network connectivity checks are triggered (#29475)
- KYOS-F: Filter conditions are added to the tray status display,and activation icons are displayed only when there are supported activation devices;The tray icon update is triggered when the device status changes to active or disconnected (#29441)
- KYOS-B: Fix translation of shortcut key types Accessibility
- KYOS-B: change ci config
- KYOS-F: New feature to restore font default settings (#20203)

* Thu Jan 18 2024 luoqing <luoqing@kylinsec.com.cn> - 2.6.0-6.kb1
- KYOS-B: Fix the issue of setting default selection sidebar errors (#24761)
- KYOS-B: Fix the issue of ineffective font changes (#25084)
- KYOS-B: change the light and dark theme name (#24747)
- KYOS-B: add custom app icon,Error prompt for modifying input shortcut keys (#25199,#25198)
- KYOS-B: profile mode using kiran-cc-daemon power interface (#25242)
- KYOS-B: Fix the space before the parameter causing a jump event when the launcher jumps (#24794)
- KYOS-B: fixed volume tray icon level display not meeting requirements (#25235)

* Wed Jan 10 2024 yanglan <yanglan@kylinsec.com.cn> - 2.6.0-5.kb4
- rebuild for KiranUI-2.6-next

* Fri Jan 05 2024 yanglan <yanglan@kylinsec.com.cn> - 2.6.0-5.kb3
- rebuild for KiranUI-2.6-next

* Tue Dec 19 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.6.0-5.kb1
- KYOS-B: set LC_TIME to UTF-8 for avoid garbled characters (#24064)

* Fri Dec 15 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.6.0-4.kb1
- KYOS-B: Fix runtime plugin loading errors

* Wed Dec 13 2023 yuanxing <yuanxing@kylinsec.com.cn> - 2.6.0-3.kb1
- KYOS-F: remove the Kiran and Adwaita icon theme in ui and translate the icon name for show

* Wed Dec 13 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.6.0-2.kb1
- KYOS-B: Fix the issue of incorrect content in the PC file provided by pkgconfig

*Tue Dec 12 2023 luoqing <luoqing@kylinsec.com.cn> - 2.6.0-1.kb1
- KYOS-F: Add AutoBoot application Settings to kiran-control-panel (#11367,#11368,#11369)
- KYOS-F: Adjust directory structure and optimize plugin implementation
- KYOS-F: add new common-widget KiranCollapse
- KYOS-F: add Spring and Summer icon theme and fix the build error of variable Names
- KYOS-F: add expand/collapse interface
- KYOS-F: add privacy policy in system information (#23443)
- KYOS-F: fix plugin-application run failed and fix some warnings.
- KYOS-F: Modify button and text spacing (#21567)
- KYOS-F: Optimize font size settings and reduce font types
- KYOS-F: The DNS configuration is optimized. Multiple DNS names are separated by semicolons (#20293)
- KYOS-F: Add the Internet connectivity check; Add the case that the network is connected but cannot access the Internet (#20294)
- KYOS-F: Volume tray icon optimization (#20296)
- KYOS-F: supports sound card selection (#20196)
- KYOS-F: After the volume back-end is set to mute, the volume value is no longer set to zero. Therefore, after clicking the mute button, the volume is set to 0
- KYOS-F: Merge the volume translation file into kiran-control-panel.zh_CN.ts
- KYOS-F: Fixed incomplete display of network details and added logs (#22716)
- KYOS-F: The cable network configuration mode is adjusted;Wireless network interface adjustment (#20295,#22263)
- KYOS-F: Modify the audio and network code according to the review
- KYOS-F: fix some build problems


* Wed Dec 06 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.5-9.kb1
- rebuild for KiranUI-2.6-next
- KYOS-B: Fixed a flash issue when the tray position changed from bottom to top
- KYOS-B: Fixed an issue where the popup was in the wrong position
- KYOS-B: Fixed an issue where the size Settings of widgets that are not currently displaying pages in the stackwidget did not take effect when multiple nics were present during tray initialization
- KYOS-B: Fixed the tray menu missing voice and network icon in the lower right corner
- KYOS-B: Modify DNS Settings and display DNS details
- KYOS-B: Improved the network notification logic
- KYOS-B: Fixed an issue where the tray icon was not displayed

* Wed Sep 13 2023 yinhongchang <yinhongchang@kylinsec.com.cn> - 2.5.5-8.kb1
- KYOS-F: fits the Qt5.9.7 interface(#15019)

* Wed Sep 13 2023 luoqing <luoqing@kylinsec.com.cn> - 2.5.5-7.kb1
- KYOS-F: The Sink/Source device list displays only active ports. The port selection function is temporarily disabled(#13261)

* Wed Sep 13 2023 luoqing <luoqing@kylinsec.com.cn> - 2.5.5-6.kb1
- KYOS-F: Fixed an issue where the volume tray icon did not change with the volume after the default Sink was changed(#14124)

* Wed Sep 13 2023 luoqing <luoqing@kylinsec.com.cn> - 2.5.5-5.kb1
- KYOS-F: Fix parsing error when json contains Chinese character(#13261)
- KYOS-F: When the gateway setting item is 0.0.0.0, the gateway setting item is null(#13150)

* Wed Sep 13 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.5-4.kb1
- KYOS-F: Fixed issues related to creating and deleting users in account management(#13994,#13990)

* Wed Sep 13 2023 yuanxing <yuanxing@kylinsec.com.cn> - 2.5.5-3.kb1
- KYOS-F: search system wallpaper xml path from kiran-background-properties
- KYOS-F: don't call xmlWriter if system background xml not exists to solve segmentation fault

* Wed Sep 13 2023 luoqing <luoqing@kylinsec.com.cn> - 2.5.5-2.kb1
- KYOS-F: After receiving the Connection::Update signal from an active connection, the connection is no longer automatically reactivated (#13231)
- KYOS-F: When switching resolutions, refresh rate preferentially selects the recommended refresh rate (#13283)

* Tue Sep 12 2023 yinhongchang <yinhongchang.kylinsec.com.cn> - 2.5.5-1.kb1
- update version to 2.5.5

* Fri Jun 16 2023 kpkg <kpkg.kylinsec.com.cn> - 2.5.3-2.kb2
- rebuild for KiranUI-2.5-next

* Fri Jun 16 2023 kpkg <kpkg.kylinsec.com.cn> - 2.5.3-2.kb1
- rebuild for KiranUI-2.5-next

* Wed May 31 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.3-2
- KYOS-B: Fix problem rename group name input box display incomplete(#67654)
- KYOS-B: Update the DBus invocation method
- KYOS-B: Verify the current password when logging features(#I795QI)

* Wed May 24 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.3-1
- KYOS-F: update auth manager translation file
- KYOS-F: Adjust the auth background color and image
- KYOS-F: add Iris, and face authentication
- KYOS-F: add ueky auth

* Fri May 12 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.2-1
- KYOS-F: add feature color-temperature,computer-mode and some power options(#67668,#68397)

* Tue May 09 2023 luoqing <luoqing@kylinsec.com.cn> - 2.5.1-1
- KYOS-F: Fix the issue of disabled plugins displaying in the side function bar classification
- KYOS-F: Fix the issue of user groups without icons in the side function bar(#69189)

* Fri Apr 28 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.0-8
- KYOS-F: compatible with older versions, launch plugins separately, and pull up the control center(#3565)

* Wed Apr 26 2023 kpkg <kpkg.kylinsec.com.cn> - 2.5.0-7
- rebuild for KiranUI-2.5-next

* Wed Apr 26 2023 luoqing <luoqing@kylinsec.om.cn> - 2.5.0-6
- KYOS-F: Fix the issue of ineffective cloning of MAC addresses, compatible with the lower version of kf5 networkmanager qt5

* Wed Apr 26 2023 kpkg <kpkg.kylinsec.com.cn> - 2.5.0-5
- rebuild for KiranUI-2.5-next

* Wed Apr 26 2023 kpkg <kpkg.kylinsec.com.cn> - 2.5.0-4
- rebuild for KiranUI-2.5-next

* Sun Apr 23 2023 wangyucheng <wangyucheng@kylinsec.om.cn> - 2.5.0-3
- KYOS-F: Compatible for versions below 5.14

* Mon Apr 10 2023 wangyucheng <wangyucheng@kylinsec.om.cn> - 2.5.0-2
- KYOS-T: add some translation

* Wed Apr 05 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.0-1
- KYOS-F: add authentication manager and user group plugin 

* Mon Mar 27 2023 luoqing <luoqing@kylinsec.com.cn> - 2.4.1-4
- KYOS-F: Fix an error where the sound output left/right balance function fails
- KYOS-F: license information display in different colors

* Thu Jan 05 2023 luoqing <luoqing@kylinsec.com.cn> - 2.4.1-3
- KYOS-F: Modify the width of the secondary options sidebar of the network plugin
- KYOS-F: Improve the disabling of volume plugin and tray
- KYOS-F: Fix the problem that the network settings of the control center, clone MAC address settings, can't be saved after being modified and cleared

* Tue Dec 13 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.4.1-2
- KYOS-F: translation activation state

* Thu Dec 01 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.4.1-1
- KYOS-F: Omit the right end of long names in network settings
- KYOS-F: Fix the problem of expanding the label when the user name is too long
- KYOS-F: In the extended mode, when there is only one display left, the "Switch" and "Set as main display" buttons will change from disabled to popup prompt
- KYOS-F: Modify the system information page. The hardware information label is KiranLabel, which supports dynamic omission
- KYOS-F: Handle the situation that the displayed popup window cannot disappear when the tray fails to grab the keyboard and mouse when it pops up
- kYOS-F: Memory in hardware information shows total memory and available memory

* Mon Nov 14 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.4.0-3
- KYOS-F: Reconstruct the code structure
- KYOS-F: Adjust the pop-up button of account management
- KYOS-F: Filter the Exec Key when reading the exec from the desktop entry
- KYOS-F: Fix problems in valgrind memcheck
- KYOS-F: briefly displays the license information,provide access to the activation interface

* Fri Nov 04 2022 luoqing <luoqing@kylinsec.com.cn> - 2.4.0-2
- KYOS-F: Fix the problem that the tray has an unavailable widget when the device is available

* Thu Nov 03 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.4.0-1
- KYOS-F: release 2.4,new plugin interface,using color block design

* Tue Sep 06 2022 luoqing <luoqing@kylinsec.com.cn> - 2.3.5-2
- KYOS-F: Fix V-P-N self-test defects

* Mon Sep 05 2022 luoqing <luoqing@kylinsec.com.cn> - 2.3.5-1
- KYOS-F: Fix the network connection status does not change after unplugging the network cable (#I5I0AA)
- KYOS-F: Fix network tay crash (#I5OKBR)
- KYOS-F: Optimized prompt pop-up box, consistent with the overall style (#I5O86G)
- KYOS-F: Re add the network profile during installation to overwrite the original configuration

* Mon Sep 05 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.3.4-10
- KYOS-F: move some account management controls to common component library

* Thu Aug 25 2022 luoqing <luoqing@kylinsec.com.cn> - 2.3.4-9
- KYOS-F: Add labels to volume and network plugins to support automated testing
- KYOS-F: Temporarily do not overwrite the NetworkManager configuration and optimize the code

* Thu Aug 25 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.3.4-8
- KYOS-F: updates accessiblename,support automated testing

* Tue Aug 23 2022 luoqing <luoqing@kylinsec.com.cn> - 2.3.4-7
- KYOS-F: fix the problem of network and details corresponding error(#I5M0L9)
- KYOS-F: Add error prompt box when saving configuration(#I5GYQD)
- KYOS-F: fix invalid configuration created for the first time
- KYOS-F: fix crash and  wireless network disable function defects

* Thu Aug 18 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.3.4-6
- KYOS-F: account the connection singleton in the signal slot is not automatically disconnected because the receiver parameter is not added, resulting in a crash(#I5HRYF)
- KYOS-F: power ,remove invalid idle shutdown display option(#I5M336)

* Wed Aug 10 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.3.4-5
- KYOS-B: error prompt box is added with the default disappearance time(#I5HR61)
- KYOS-B: when the relevant setting interface cannot be searched, a prompt will be added(#I5H192)

* Tue Aug 09 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.3.4-4
- KYOS-F: set panel creategory widget auto fill background

* Fri Aug 05 2022 luoqing <luoqing@kylinsec.com.cn> - 2.3.4-3
- KYOS-B: fix not searching the wireless network for a long time after the WiFi connection is successful(#I5IPVO)

* Thu Aug 04 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.3.4-2
- KYOS-B: update systeminfo plugin dialog style(#I5H3YX)
- KYOS-B: buttons on system information page are not fully dispalyed(#I5H229)

* Mon Aug 01 2022 luoqing <luoqing@kylinsec.com.cn> - 2.3.4-1
- KYOS-B: fix no notification when connecting to the hidden network(#I5IS25)
- KYOS-B: the tray network icon changes as the primary connection changes(#I5IPBG)
- KYOS-B: fix the lack of operation buttons and button translation in more interfaces connected to WiFi(#I5IQ03,#I5IPY3)

* Fri Jul 29 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.3.3-1
- KYOS-B: fix the error in the animation display of the Create Account button(#I5HR7F)
- KYOS-B: forbidden to create users with uid less than 1000(#I5HZQF)
- KYOS-B: fix the problem that the user-defined shortcut key type is wrong due to adding shortcut keys(#I5IE4B)

* Fri Jul 22 2022 luoqing <luoqing@kylinsec.com.cn> - 2.3.2-1
- KYOS-B: fix some bugs of network and audio plugins

* Tue Jul 12 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.3.1-4
- KYOS-B: fix kiran control panel translator 

* Mon Jul 11 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.3.1-3
- KYOS-B: fix system infomation plugin build require

* Mon Jul 11 2022 luoqing <luoqing@kylinsec.com.cn> - 2.3.1-2
- KYOS-B: fix unplugging headphones causes crash

* Sat Jul 09 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.3.1-1
- KYOS-F: add network plugin
- KYOS-F: fix kiran-control-panel titlebar translation

* Fri Jul 08 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.3.0-3
- KYOS-F: remove useless pkgconfig file

* Fri Jul 08 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.3.0-2
- KYOS-F: temporarily shield the network plugin

* Thu Jul 07 2022 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.3.0-1
- KYOS-F: release kiran-control-panel 2.3

* Mon Jan 24 2022 longcheng <longcheng@kylinos.com.cn> - 2.2.0-2.kb3
- KYOS-B: fix(build): fix  incomplete type 'class QPainterPath' build

* Wed Jan 12 2022 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-2.kb2
- rebuild for KY3.4-5-KiranUI-2.2

* Wed Dec 29 2021 kpkg <kpkg@kylinos.com.cn> - 2.2.0-2.kb1
- rebuild for KY3.4-MATE-modules-dev

* Wed Dec 29 2021 caoyuanji<caoyuanji@kylinos.com.cn> - 2.2.0-2
- Upgrade version number for easy upgrade

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-1.kb2
- rebuild for KY3.4-4-KiranUI-2.2

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-1.kb1
- rebuild for KY3.4-4-KiranUI-2.2

* Thu Dec 02 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.2.0-1
- KYBD: build for 2.2,changed required name to kiran-widgets-qt5

* Thu Dec 02 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.1-1.kb3
- KYBD: changed required name to kiranwidgets-qt5

* Thu Nov 04 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.1-1.kb2
- KYBD: changed license and requires

* Mon Nov 01 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.１-1.kb1
- KYOS-B: fixed the problem that the selected items in the sidebar display errors
- KYOS-F: draw sidebar border

* Tue Sep 14 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.1.kb1
- KYOS-F: launcher set LC_TIME to UTF-8 (#43747)

* Wed Jun 30 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.0-3.kb1
- KYOS-B: desktop file lacks keywords key allow loading 

* Wed Jun 16 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.0-2.kb1
- KYOS-B: update the sub function list display

* Thu Jun 10 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.0-1.kb1
- KYOS-B: fix stacking icons on the bottom panel of plugins

* Wed Jun 09 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.0-2.kb1
- KYOS-B: rename categorys

* Tue Jun 08 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.0-1.kb2
- KYOS-B: fix dependency name error

* Mon Jun 07 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.0-1.kb1
- Package init

